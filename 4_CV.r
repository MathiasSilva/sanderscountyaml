
#10-fold CV for OLS on full model
ols1.MSE.in <- rep(0,10)
ols1.MSE.out <- rep(0,10)
for(i in 1:10){
    ols1.cv.fit <- glm(formula1,data=data[eval(as.name(paste("train",i,sep=""))),])
    ols1.MSE.in[i] <- mean((data[eval(as.name(paste("train",i,sep=""))),"fraction_votes"]-predict(ols1.cv.fit,newdata=data[eval(as.name(paste("train",i,sep=""))),]))^2)
    ols1.MSE.out[i] <- mean((data[eval(as.name(paste("test",i,sep=""))),"fraction_votes"]-predict(ols1.cv.fit,newdata=data[eval(as.name(paste("test",i,sep=""))),]))^2)
    }
ols1.MSE <- c(mean(ols1.MSE.in),mean(ols1.MSE.out))
ols1.train <- glm(formula1,data=data[eval(as.name(paste("train",1,sep=""))),])
#10-fold CV for OLS on FSS selected model
ols.fss.MSE.in <- rep(0,10)
ols.fss.MSE.out <- rep(0,10)
for(i in 1:10){
    ols.fss.cv.fit <- glm(ols1.fss.formula,data=data[eval(as.name(paste("train",i,sep=""))),])
    ols.fss.MSE.in[i] <- mean((data[eval(as.name(paste("train",i,sep=""))),"fraction_votes"]-predict(ols.fss.cv.fit,newdata=data[eval(as.name(paste("train",i,sep=""))),]))^2)
    ols.fss.MSE.out[i] <- mean((data[eval(as.name(paste("test",i,sep=""))),"fraction_votes"]-predict(ols.fss.cv.fit,newdata=data[eval(as.name(paste("test",i,sep=""))),]))^2)
    }
ols.fss.MSE <- c(mean(ols.fss.MSE.in),mean(ols.fss.MSE.out))
ols.fss.train <- glm(ols1.fss.formula,data=data[eval(as.name(paste("train",1,sep=""))),])
#10-fold CV for OLS on BkSS selected model
ols.bkss47.MSE.in <- rep(0,10)
ols.bkss47.MSE.out <- rep(0,10)
for(i in 1:10){
    ols.bkss47.cv.fit <- glm(ols1.bkss.formula47,data=data[eval(as.name(paste("train",i,sep=""))),])
    ols.bkss47.MSE.in[i] <- mean((data[eval(as.name(paste("train",i,sep=""))),"fraction_votes"]-predict(ols.bkss47.cv.fit,newdata=data[eval(as.name(paste("train",i,sep=""))),]))^2)
    ols.bkss47.MSE.out[i] <- mean((data[eval(as.name(paste("test",i,sep=""))),"fraction_votes"]-predict(ols.bkss47.cv.fit,newdata=data[eval(as.name(paste("test",i,sep=""))),]))^2)    
    }
ols.bkss47.MSE <- c(mean(ols.bkss47.MSE.in),mean(ols.bkss47.MSE.out))
ols.bkss47.train <- glm(ols1.bkss.formula47,data=data[eval(as.name(paste("train",1,sep=""))),])
#10-fold CV for Ridge parameter:
set.seed(12345)
cv.out=cv.glmnet(ridge.x[train1,],ridge.y[train1],alpha=0,lambda=lambda_grid)
best.ridge.train <- glmnet(ridge.x[train1,],ridge.y[train1],alpha=0,lambda=cv.out$lambda.min)
#Best lambda=0.0077: cv.out$lambda.min
#coefficients: coef(best.ridge.train)
#10-fold CV for Ridge
ridge.MSE.in <- rep(0,10)
ridge.MSE.out <- rep(0,10)
for(i in 1:10){
ridge.cv.fit <- glmnet(ridge.x[eval(as.name(paste("train",i,sep=""))),],ridge.y[eval(as.name(paste("train",i,sep="")))],lambda=cv.out$lambda.min,alpha=0)
    ridge.MSE.in[i] <- mean((ridge.y[eval(as.name(paste("train",i,sep="")))]-predict(ridge.cv.fit,newx=ridge.x[eval(as.name(paste("train",i,sep=""))),]))^2)
    ridge.MSE.out[i] <- mean((ridge.y[eval(as.name(paste("test",i,sep="")))]-predict(ridge.cv.fit,newx=ridge.x[eval(as.name(paste("test",i,sep=""))),]))^2)
    }
ridge.MSE <- c(mean(ridge.MSE.in),mean(ridge.MSE.out))
#10-fold CV for PCR number of components:
set.seed(12345)
#Number of components that minimizes CV MSE
pcr.train <- pcr(ridge.y[train1]~ridge.x[train1,],scale=FALSE,validation="CV",intercept=TRUE)
pcrCVMSE <- function(x){mean((x-ridge.y[train1])^2)}
pcr.train.CVMSE <- c(mean((mean(ridge.y[train1])-ridge.y[train1])^2),apply(pcr.train$validation$pred[,1,1:51],2,FUN=pcrCVMSE))
pcr.train.bestM <- names(pcr.train.CVMSE)[which(pcr.train.CVMSE==min(pcr.train.CVMSE))]
#Best M=50 : pcr.train.bestM
#coefficients: coef(pcr.train,intercept=TRUE,50)
#10-fold CV for PCR
pcr.MSE.in <- rep(0,10)
pcr.MSE.out <- rep(0,10)
for(i in 1:10){
pcr.cv.fit <- pcr(ridge.y[eval(as.name(paste("train",i,sep="")))]~ridge.x[eval(as.name(paste("train",i,sep=""))),],scale=FALSE,ncomp=50,intercept=TRUE)
pcr.MSE.in[i] <- mean((ridge.y[eval(as.name(paste("train",i,sep="")))]-predict(pcr.cv.fit,newdata=ridge.x[eval(as.name(paste("train",i,sep=""))),],ncomp=50))^2)
pcr.MSE.out[i] <- mean((ridge.y[eval(as.name(paste("test",i,sep="")))]-predict(pcr.cv.fit,newdata=ridge.x[eval(as.name(paste("test",i,sep=""))),],ncomp=50))^2)
    }
pcr.MSE <- c(mean(pcr.MSE.in),mean(pcr.MSE.out))

#Cross validation to select optimal lambda for lasso regression:
lasso.x <- model.matrix(ols1)[,-1]
lasso.y=data$fraction_votes
set.seed(12345)
cv_lasso<-cv.glmnet(lasso.x[train1,],lasso.y[train1], alpha=1,lambda=lambda_grid)
lambda_lasso<-cv_lasso$lambda.min
#coefficients:predict(cv_lasso, type="coefficients", s=lambda_lasso)
#10-fold CV for the Lasso
lasso.MSE.in <- rep(0,10)
lasso.MSE.out <- rep(0,10)
for(i in 1:10){
lasso.cv.fit<-glmnet(lasso.x[eval(as.name(paste("train",i,sep=""))),], lasso.y[eval(as.name(paste("train",i,sep="")))], alpha = 1,lambda=lambda_lasso)
lasso.MSE.in[i]<-mean((data[eval(as.name(paste("train",i,sep=""))),"fraction_votes"]-predict(lasso.cv.fit,newx=lasso.x[eval(as.name(paste("train",i,sep=""))),]))^2)
lasso.MSE.out[i]<-mean((data[eval(as.name(paste("test",i,sep=""))),"fraction_votes"]-predict(lasso.cv.fit,newx=lasso.x[eval(as.name(paste("test",i,sep=""))),]))^2)
}
lasso.MSE <- c(mean(lasso.MSE.in),mean(lasso.MSE.out))

#ADAPTIVE LASSO
#specify omega
w<-1/abs(matrix(coef(ols1))) #gamma=1 here
#10-fold CV for Adaptative Lasso
cv_alasso=cv.glmnet(ridge.x[train1,],ridge.y[train1],alpha=1,type.measure="mse",penalty.factor=w,lambda=lambda_grid)
lambda_lasso2=cv_alasso$lambda.min
set.seed(12345)
best_alasso_train<-glmnet(ridge.x[train1,],ridge.y[train1],alpha=1,lambda=lambda_lasso2,penalty.factor = w)
#coefficients: coef(best_alasso_train)
alasso.MSE.in <- rep(0,10)
alasso.MSE.out <- rep(0,10)
for(i in 1:10){
  alasso.cv.fit<-glmnet(lasso.x[eval(as.name(paste("train",i,sep=""))),], lasso.y[eval(as.name(paste("train",i,sep="")))], alpha = 1, lambda=lambda_lasso2,penalty.factor = w)
  alasso.MSE.in[i]<-mean((data[eval(as.name(paste("train",i,sep=""))),"fraction_votes"]-predict(alasso.cv.fit,newx=lasso.x[eval(as.name(paste("train",i,sep=""))),]))^2)
  alasso.MSE.out[i]<-mean((data[eval(as.name(paste("test",i,sep=""))),"fraction_votes"]-predict(alasso.cv.fit,newx=lasso.x[eval(as.name(paste("test",i,sep=""))),]))^2)
}
alasso.MSE <- c(mean(alasso.MSE.in),mean(alasso.MSE.out))

#ELASTIC NET
#specify a grid for values of alpha to test
grid_alpha<-seq(0,1,by=0.01)
#Specify optimal alpha and lambda using 10-fold CV:
x<-grid_alpha
yhat<-function(x){
  aux1<-cv.glmnet(ridge.x[train1,], ridge.y[train1], type.measure = "mse",
                  alpha=x,lambda=lambda_grid)
  return(c(aux1$lambda.min, mean((ridge.y[test1]-predict(aux1, s=aux1$lambda.min, newx=ridge.x[test1,]))^2)))}
msenet<-sapply(x, yhat)
alpha_net<-grid_alpha[which(msenet[2,]==min(msenet[2,]))]
lambda_net<-msenet[1, which(msenet[2,]==min(msenet[2,]))]
#elastic net regression
netreg<-glmnet(lasso.x[train1,], lasso.y[train1], alpha=alpha_net, lambda=lambda_net)
#10-fold CV for the Elastic Net
net.MSE.in <- rep(0,10)
net.MSE.out <- rep(0,10)
for(i in 1:10){
  net.cv.fit<-glmnet(lasso.x[eval(as.name(paste("train",i,sep=""))),], lasso.y[eval(as.name(paste("train",i,sep="")))], alpha=alpha_net, lambda = lambda_net)
  net.MSE.in[i]<-mean((data[eval(as.name(paste("train",i,sep=""))),"fraction_votes"]-predict(net.cv.fit,newx=lasso.x[eval(as.name(paste("train",i,sep=""))),]))^2)
  net.MSE.out[i]<-mean((data[eval(as.name(paste("test",i,sep=""))),"fraction_votes"]-predict(net.cv.fit,newx=lasso.x[eval(as.name(paste("test",i,sep=""))),]))^2)
}
net.MSE <- c(mean(net.MSE.in),mean(net.MSE.out))

#GETS
set.seed(12345)
gets1 <- getsm(arx(y=ridge.y[train1],mx=ridge.x[train1,],mc=TRUE),ar.LjungB=NULL,arch.LjungB=NULL,normality.JarqueB=NULL)
#colnames(ridge.x)[paths(gets1)[[6]]]
#paths(gets1)
#terminals(gets1)
#Eliminated variables in best terminal model: colnames(ridge.x)[colnames(ridge.x)%in%names(coef(gets1))==FALSE]
gets.data <- cbind.data.frame("y"=ridge.y,ridge.x[,names(coef(gets1))])
ols.gets.best <- glm(y~.,data=gets.data[eval(as.name(paste("train",1,sep=""))),])
gets.MSE.in <- rep(0,10)
gets.MSE.out <- rep(0,10)
for(i in 1:10){
ols.gets <- glm(y~.,data=gets.data[eval(as.name(paste("train",i,sep=""))),])
  gets.MSE.in[i]<-mean((gets.data[eval(as.name(paste("train",i,sep=""))),"y"]-predict(ols.gets,newdata=gets.data[eval(as.name(paste("train",i,sep=""))),]))^2)
  gets.MSE.out[i]<-mean((gets.data[eval(as.name(paste("test",i,sep=""))),"y"]-predict(ols.gets,newdata=gets.data[eval(as.name(paste("test",i,sep=""))),]))^2)
}
gets.MSE <- c(mean(gets.MSE.in),mean(gets.MSE.out))


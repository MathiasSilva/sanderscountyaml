rm(list=ls())
library(leaps)
library(dummies)
library(boot)
library(FactoMineR)
library(car)
library(glmnet)
library(pls)
library(stargazer)
library(gets)
raw_data_dir <- "C:/Users/Jakob/Documents/Master Marseille/Master 2/Gitty/master_2/Semester 2/Automatic model selection methods/Project/Data"
scripts_dir <- "C:/Users/Jakob/Documents/Master Marseille/Master 2/Gitty/master_2/Semester 2/Automatic model selection methods/Project/Code_final"
setwd(raw_data_dir)

#1) Load data and clearing
source(file=paste(scripts_dir,"1_dataproc.r",sep="/"))

#2) OLS model estimate and variable selection
source(file=paste(scripts_dir,"2_ols.r",sep="/"))
#Note: If this second script doesn't work it might be necessary to run the previous script (1_dataproc.r) directly before, not using the source() call.

#3) Ridge and PCR estimates
source(file=paste(scripts_dir,"3_ridgepcr.r",sep="/"))

#4) MSE and CV
source(file=paste(scripts_dir,"4_CV.r",sep="/"))

#N) Additional output
source(file=paste(scripts_dir,"n_output.r",sep="/"))


full.pca <- PCA(data[,c("ele_demvotpart","pop_density_10","pop_fem_14","pop_whiteonly_14","pop_over65_14","pop_foreignborn_0913","pop_vetprop_0913","pop_overbachelors_0913","eco_hownership_0913","eco_poverty_0913","eco_nfemp_13","eco_firms_07")],graph=F)

#Percentage of variance per PC:
#round(full.pca$eig,2)

#Ridge regression
lambda_grid=c(seq(0,10^-2,by=0.0001),10^seq(10,-2,length=100))
ridge.x <- model.matrix(ols1)[,-1]
ridge.y=data$fraction_votes
ridge.mod=glmnet(ridge.x,ridge.y,alpha=0,lambda=lambda_grid)

#PCR - no scaling done due to very low sd on some state dummies
set.seed(12345)
pcreg <- pcr(ridge.y~ridge.x,scale=FALSE,validation="CV",intercept=TRUE)
#validationplot(pcreg,val.type="MSEP")

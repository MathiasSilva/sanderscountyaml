formula1 <- as.formula(paste("fraction_votes~ele_demvotpart+I(log(pop_density_10))+pop_fem_14+pop_whiteonly_14+pop_over65_14+pop_foreignborn_0913+pop_vetprop_0913+pop_overbachelors_0913+eco_hownership_0913+eco_poverty_0913+eco_nfemp_13+I(log(eco_firms_07+1))",paste0(colnames(state_dummies)[-1],sep="",collapse="+"),sep="+",collapse="+"))
ols1 <- glm(formula1,data=data)

################################################################
#FSS
################################################################
fss.ols1 <- regsubsets(formula1,data[train1,],really.big=T,nvmax=63,method="forward")
summary.fss1 <- summary(fss.ols1)

#Diagnostic plots using BIC or adj r2:
#plot(summary.fss1$adjr2,xlab=" Number of Variables",ylab="Adjusted RSq",type="l")

#Best model:
which(summary.fss1$adjr2==max(summary.fss1$adjr2))
which(summary.fss1$bic==min(summary.fss1$bic))
#48 variables according to r-sq adjusted and 46 variables according to BIC criteria, omitted:
#names(coef(fss.ols1,51))[names(coef(fss.ols1,51))%in%names(coef(fss.ols1,48))==FALSE]
#names(coef(fss.ols1,51))[names(coef(fss.ols1,51))%in%names(coef(fss.ols1,46))==TRUE]

ols1.fss.formula <- as.formula(paste("fraction_votes~",paste0(names(coef(fss.ols1,51))[names(coef(fss.ols1,51))%in%names(coef(fss.ols1,48))==TRUE][-1],sep="",collapse="+"),sep="",collapse=""))
ols1.fss <- glm(ols1.fss.formula,data=data[train1,])
################################################################
#Backward Stepwise Selection (BkSS)
################################################################
bkss.ols1 <- regsubsets(formula1,data[train1,],really.big=T,nvmax=63,method="backward")
summary.bkss1 <- summary(bkss.ols1)

#Diagnostic plots using BIC or adj r2:
#plot(summary.bkss1$adjr2,xlab=" Number of Variables",ylab="Adjusted RSq",type="l")

#Best model:
which(summary.bkss1$adjr2==max(summary.bkss1$adjr2))
which(summary.bkss1$bic==min(summary.bkss1$bic))
#47 variables according to r-sq adjusted and 42 according to BIC criteria, omitted using r-sq adjusted:
#names(coef(bkss.ols1,51))[names(coef(bkss.ols1,51))%in%names(coef(bkss.ols1,47))==FALSE]
#omitted using BIC adjusted:
#names(coef(bkss.ols1,51))[names(coef(bkss.ols1,51))%in%names(coef(bkss.ols1,42))==FALSE]
#Included:
#names(coef(bkss.ols1,51))[names(coef(bkss.ols1,51))%in%names(coef(bkss.ols1,47))==TRUE]

#Candidate models according to this stepwise selection criteria:
ols1.bkss.formula47 <- as.formula(paste("fraction_votes~",paste0(names(coef(bkss.ols1,51))[names(coef(bkss.ols1,51))%in%names(coef(bkss.ols1,47))==TRUE][-1],sep="",collapse="+"),sep="",collapse=""))

ols1.bkss47 <- glm(ols1.bkss.formula47,data=data[train1,]) 

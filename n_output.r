#Regression output for glm() estimates (PCR and Ridge coefficients are added by hand):
stargazer(ols1.train,ols.fss.train,ols.bkss47.train)

#MSE section for regression output table:
MSE <- t(rbind(ols1.MSE,ols.fss.MSE,ols.bkss47.MSE,ridge.MSE,pcr.MSE,lasso.MSE,alasso.MSE,net.MSE,gets.MSE))

#Variance Inflation Factor on glm() models on full data fits
#c(max(vif(ols1)),max(vif(ols1.fss)),max(vif(ols1.bkss43)),max(vif(ols1.bkss47)),max(vif(ols.gets.best)))

#FSS on full data diagnostic plot
png("fssplot1.png", width = 800, height = 600)
par(mar=c(5, 4, 4, 6) + 0.1)
## Plot first set of data and draw its axis
plot(1:51, summary.fss1$adjr2, axes=FALSE, xlab="", ylab="",type="l",col="black", main="FSS")
axis(2,col="black",las=1)  ## las=1 makes horizontal labels
mtext("Adjusted R2",side=2,line=2.5)
box()
## Allow a second plot on the same graph
par(new=TRUE)

## Plot the second plot and put axis scale on right
plot(1:51, summary.fss1$bic,xlab="", ylab="",axes=FALSE, type="l", col="red")
## a little farther out (line=3.5) to make room for labels
mtext("BIC",side=4,col="red",line=3.5) 
axis(4, col="red",col.axis="red",las=1)
axis(1,1:51)
mtext("Variables",side=1,col="black",line=2.5)
abline(v=which(summary.fss1$adjr2==max(summary.fss1$adjr2)),lty=2)
abline(v=which(summary.fss1$bic==min(summary.fss1$bic)),lty=2)
dev.off()
#BkSS on full data diagnostic plot
png("bkssplot1.png", width = 800, height = 600)
par(mar=c(5, 4, 4, 6) + 0.1)
## Plot first set of data and draw its axis
plot(1:51, summary.bkss1$adjr2, axes=FALSE, xlab="", ylab="",type="l",col="black", main="BkSS")
axis(2,col="black",las=1)  ## las=1 makes horizontal labels
mtext("Adjusted R2",side=2,line=2.5)
box()
## Allow a second plot on the same graph
par(new=TRUE)
## Plot the second plot and put axis scale on right
plot(1:51, summary.bkss1$bic,xlab="", ylab="",axes=FALSE, type="l", col="red")
## a little farther out (line=3.5) to make room for labels
mtext("BIC",side=4,col="red",line=3.5) 
axis(4, col="red",col.axis="red",las=1)
axis(1,1:51)
mtext("Variables",side=1,col="black",line=2.5)
abline(v=which(summary.bkss1$adjr2==max(summary.bkss1$adjr2)),lty=2)
abline(v=which(summary.bkss1$bic==min(summary.bkss1$bic)),lty=2)
dev.off()

#Validation plot on PCR fitted on training set
png("pcrplot1.png", width = 800, height = 600)
plot(0:51,pcr.train.CVMSE[1:52],ylim=c(min(pcr.train.CVMSE),max(pcr.train.CVMSE[1:51])),main="PCR 10-fold MSE",type="h",ylab="MSE",xlab="Number of Components")
points(0:51,pcr.train.CVMSE[1:52])
dev.off()
